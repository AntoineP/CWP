<!DOCTYPE html>
<html>
<head>
<title>CUPS Web Printing</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="./cwp-style.css">
</head>

<body>

<h1>CUPS Web Printing</h1>

<?php
  // file describing printers and available options
  $settings["printers_file"] = "./printers-template.json";
  // file containing strings and translations of CWP interface and
  // printer options
  $settings["strings_file"] = "./cwp-strings.json";
  // default language to use when no language is preferred by browser or
  // when no translation is available (2 or 3 letters code)
  $settings["default_lang"] = "en";
  // retrieve printers status
  $settings["retrieve_printers_status"] = true;
  // display status and number of jobs in queue for each printer
  $settings["show_printers_status"] = true;
  // enable printing only on available printers
  $settings["disable_faulty_printers"] = false;
  // maximum number of copies for one job. Set to "0" to disable.
  $settings["max_copies"] = 50;
  // maximum size of the file to print (in MiB, 1 048 576 bytes is 1MiB)
  // to be more restrictive than global PHP config. Set to "0" to disable
  $settings["max_file_size"] = 5;
  // string used as job username if user is not authenticated.
  $settings["default_username"] = "cwp";
  // print some debug output
  $settings["debug"] = true;

  // functions file
  require "./cwp-functions.php";
  Main($settings);
?>

</body>

</html>
